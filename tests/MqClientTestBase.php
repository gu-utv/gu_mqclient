<?php

namespace gu\mqclient\tests;

use phpunit\Framework\TestCase;

/**
 * Base test for the MQ Client.
 */
class MqClientTestBase extends TestCase {

  /**
   * Gets the protected attributes property of an object.
   *
   * @param mixed $object
   *   An object that has a protected 'attributes' property.
   *
   * @return array
   *   The attributes property, which is always an array.
   */
  protected function getAttributesOfObject($object): array {
    $reflector = new \ReflectionClass(get_class($object));
    $property = $reflector->getProperty('attributes');
    $property->setAccessible(TRUE);
    return $property->getValue($object);
  }

  /**
   * Sets the attributes protected property of an object.
   *
   * @param mixed $object
   *   An object that has a protected 'attributes' property.
   * @param array $value
   *   The new value of the 'attributes' property.
   */
  protected function setAttributesOfObject($object, array $value) {
    $reflector = new \ReflectionClass(get_class($object));
    $property = $reflector->getProperty('attributes');
    $property->setAccessible(TRUE);
    $property->setValue($object, $value);
  }

}
