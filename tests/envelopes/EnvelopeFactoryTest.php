<?php

namespace gu\mqclient\tests\envelopes;

use gu\mqclient\envelopes\EnvelopeFactory;
use gu\mqclient\envelopes\EnvelopeInterface;
use gu\mqclient\envelopes\MqcnoEnvelopeInterface;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\stamps\MqStamp;
use gu\mqclient\stamps\StampInterface;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the envelope Factory Class.
 */
class EnvelopeFactoryTest extends MqClientTestBase {

  /**
   * Test creating a generic envelope.
   */
  public function testCreate() {
    $factory = new EnvelopeFactory();
    $envelope = $factory->create(new MqStamp());
    $this->assertInstanceOf(EnvelopeInterface::class, $envelope);
    $this->assertInstanceOf(MqStamp::class, $envelope->getFirstStamp());
  }

  /**
   * Test creating a object envelope.
   *
   * @param bool $readonly
   *   The readonly parameter.
   *
   * @dataProvider readonlyProvider
   */
  public function testCreateObjectEnvelope(bool $readonly) {
    $factory = new EnvelopeFactory();
    $envelope = $factory->createOpenObjectEnvelope(new MqStamp(), $readonly);
    $this->assertInstanceOf(MqodEnvelopeInterface::class, $envelope);
    $this->assertInstanceOf(StampInterface::class, $envelope->getFirstStamp());
    if ($readonly) {
      $this->assertGreaterThan(0, $envelope->getOptions() & MQSERIES_MQOO_INPUT_SHARED);
      $this->AssertEquals(0, $envelope->getOptions() & MQSERIES_MQOO_OUTPUT);
    }
    else {
      $this->AssertEquals(0, $envelope->getOptions() & MQSERIES_MQOO_INPUT_SHARED);
      $this->assertGreaterThan(0, $envelope->getOptions() & MQSERIES_MQOO_OUTPUT);
    }
  }

  /**
   * Test creating a connection envelope.
   */
  public function testCreateConnectionEnvelope() {
    $factory = new EnvelopeFactory();
    $envelope = $factory->createConnectionEnvelope(new MqStamp(), 'qm');
    $this->assertInstanceOf(MqcnoEnvelopeInterface::class, $envelope);
    $this->assertInstanceOf(StampInterface::class, $envelope->getFirstStamp());
    $this->assertEquals('qm', $envelope->getQueueManager());
  }

  /**
   * Data provider for testCreateObjectEnvelope.
   */
  public function readonlyProvider() {
    return [
      [TRUE],
      [FALSE],
    ];
  }

}
