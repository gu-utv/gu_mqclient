<?php

namespace gu\mqclient\tests\envelopes;

use gu\mqclient\envelopes\MqgmoEnvelopeInterface;
use gu\mqclient\envelopes\MqodEnvelope;
use gu\mqclient\envelopes\MqpmoEnvelopeInterface;
use gu\mqclient\stamps\MqcdStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQOD envelope.
 */
class MqodEnvelopeTest extends MqClientTestBase {

  /**
   * Test for the constructor function.
   */
  public function testConstructor() {
    $stamp = new MqcdStamp();
    $envelope = new MqodEnvelope($stamp, TRUE);
    $this->assertGreaterThan(0, $envelope->getOptions() & MQSERIES_MQOO_INPUT_SHARED);
    $this->assertEquals(0, $envelope->getOptions() & MQSERIES_MQOO_OUTPUT);
    $envelope = new MqodEnvelope($stamp, FALSE);
    $this->assertGreaterThan(0, $envelope->getOptions() & MQSERIES_MQOO_OUTPUT);
    $this->assertEquals(0, $envelope->getOptions() & MQSERIES_MQOO_INPUT_SHARED);
  }

  /**
   * Test toGetEnvelope.
   *
   * @param string $queue
   *   An open queue resource, mocked to a string for testing.
   * @param bool $sync_point
   *   Whether to enable a sync_point.
   *
   * @dataProvider dataProvider
   */
  public function testToGetEnvelope($queue, bool $sync_point) {
    $stamp = new MqcdStamp();
    $envelope = new MqodEnvelope($stamp);
    $envelope->setOpenObject($queue);
    $mqgmo = $envelope->toGetEnvelope($sync_point);
    $this->assertInstanceOf(MqgmoEnvelopeInterface::class, $mqgmo);
    $this->assertEquals($queue, $mqgmo->getQueue());
    $stamp = $mqgmo->getFirstStamp();
    if ($sync_point) {
      $this->assertGreaterThan(0, $stamp->getAttribute('Options') & MQSERIES_MQGMO_SYNCPOINT);
    }
    else {
      $this->assertEquals(0, $stamp->getAttribute('Options') & MQSERIES_MQGMO_SYNCPOINT);
    }
  }

  /**
   * Test toPutEnvelope.
   *
   * @param string $queue
   *   An open queue resource, mocked to a string for testing.
   * @param bool $sync_point
   *   Whether to enable a sync_point.
   *
   * @dataProvider dataProvider
   */
  public function testToPutEnvelope($queue, bool $sync_point) {
    $stamp = new MqcdStamp();
    $envelope = new MqodEnvelope($stamp);
    $envelope->setOpenObject($queue);
    $mqpmo = $envelope->toPutEnvelope($sync_point);
    $this->assertInstanceOf(MqpmoEnvelopeInterface::class, $mqpmo);
    $this->assertEquals($queue, $mqpmo->getQueue());
    $stamp = $mqpmo->getFirstStamp();
    if ($sync_point) {
      $this->assertGreaterThan(0, $stamp->getAttribute('Options') & MQSERIES_MQPMO_SYNCPOINT);
    }
    else {
      $this->assertEquals(0, $stamp->getAttribute('Options') & MQSERIES_MQPMO_SYNCPOINT);
    }
  }

  /**
   * Data provider for two tests.
   *
   * @return array
   *   The provided data.
   */
  public function dataProvider() {
    return [
      ['queue1', TRUE],
      ['queue2', FALSE],
    ];
  }

}
