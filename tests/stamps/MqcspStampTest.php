<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqcspStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQCSP Stamp.
 */
class MqcspStampTest extends MqClientTestBase {

  /**
   * Test setting the authetication information.
   */
  public function testSetAuth() {
    $stamp = new MqcspStamp();
    $stamp->setAuth('user', 'pass');
    $this->assertEquals('user', $stamp->getAttribute('CSPUserId'));
    $this->assertEquals('pass', $stamp->getAttribute('CSPPassword'));
    $this->assertEquals(\MQSERIES_MQCSP_AUTH_USER_ID_AND_PWD, $stamp->getAttribute('AuthenticationType'));
  }

  /**
   * Test unsetting the authetication information.
   */
  public function testUnsetAuth() {
    $stamp = new MqcspStamp();
    $stamp->setAuth('user', 'pass');
    $stamp->unsetAuth();
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayNotHasKey('CSPUserId', $attributes);
    $this->assertArrayNotHasKey('CSPPassword', $attributes);
    $this->assertEquals($attributes['AuthenticationType'], \MQSERIES_MQCSP_AUTH_NONE);
  }

}
