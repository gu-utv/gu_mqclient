<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqcdStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQCD Stamp.
 */
class MqcdStampTest extends MqClientTestBase {

  /**
   * Test for the constructor.
   */
  public function testConstructor() {
    $stamp = new MqcdStamp();
    $this->assertGreaterThanOrEqual(5, $stamp->getAttribute('Version'));
  }

  /**
   * Test for ebabling SSL.
   */
  public function testEnableSsl() {
    $stamp = new MqcdStamp();
    $stamp->enableSsl();
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertEquals('TLS_RSA_WITH_AES_128_CBC_SHA256', $attributes['SSLCipherSpec']);
    $this->assertEquals(MQSERIES_MQSCA_REQUIRED, $attributes['SSLClientAuth']);
  }

  /**
   * Test for disabling SSL.
   */
  public function testDisableSsl() {
    $stamp = new MqcdStamp();
    $stamp->enableSsl();
    $stamp->disableSsl();
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayNotHasKey('SSLCipherSpec', $attributes);
    $this->assertArrayNotHasKey('SSLClientAuth', $attributes);
  }

  /**
   * Test setting the container name.
   */
  public function testSetConnectionName() {
    $stamp = new MqcdStamp();
    $stamp->setConnectionName('host', 100);
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayHasKey('ConnectionName', $attributes);
    $this->assertEquals('host(100)', $attributes['ConnectionName']);
  }

  /**
   * Test setting the channel name.
   */
  public function testSetChannelName() {
    $stamp = new MqcdStamp();
    $stamp->setChannelName('channel');
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayHasKey('ChannelName', $attributes);
    $this->assertEquals('channel', $attributes['ChannelName']);
  }

}
