<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqgmoStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQGMO Stamp.
 */
class MqgmoStampTest extends MqClientTestBase {

  /**
   * Test enabling a sync point.
   */
  public function testEnableSyncPoint() {
    $stamp = new MqgmoStamp();
    $this->assertEquals(0, $stamp->getAttribute('Options') & \MQSERIES_MQGMO_SYNCPOINT, 'Sync point is disabled by default.');
    $stamp->enableSyncPoint();
    $this->assertGreaterThan(0, $stamp->getAttribute('Options') & \MQSERIES_MQGMO_SYNCPOINT, 'Sync point is enabled.');
  }

  /**
   * Test enabling wait for a message.
   */
  public function testEnableWait() {
    $stamp = new MqgmoStamp();
    $this->assertEquals(0, $stamp->getAttribute('Options') & \MQSERIES_MQGMO_WAIT, 'Wait is disabled by default.');
    $stamp->enableWait(10);
    $this->assertGreaterThan(0, $stamp->getAttribute('Options') & \MQSERIES_MQGMO_WAIT, 'Wait is enabled.');
    $this->assertEquals(10, $stamp->getAttribute('WaitInterval'));
  }

}
