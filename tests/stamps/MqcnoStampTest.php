<?php

namespace gu\mqclient\tests\stamps;

// Injecting MQ Series mock functions to test the connection.
include_once __DIR__ . '/../MqseriesMockFunctions.php';

use gu\mqclient\envelopes\MqcnoEnvelope;
use gu\mqclient\MqConnection;
use gu\mqclient\stamps\MqcdStamp;
use gu\mqclient\stamps\MqcnoStamp;
use gu\mqclient\stamps\MqcspStamp;
use gu\mqclient\stamps\MqscoStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQCNO Stamp.
 */
class MqcnoStampTest extends MqClientTestBase {

  /**
   * Constructor test.
   */
  public function testConstructor() {
    $stamp = new MqcnoStamp();
    $this->assertGreaterThanOrEqual(5, $stamp->getAttribute('Version'));
    $this->assertInstanceOf(MqcdStamp::class, $stamp->getAttribute('MQCD'), 'Default MQCD created by the constructor');
  }

  /**
   * Test enabling SSL.
   */
  public function testEnableSsl() {
    $stamp = new MqcnoStamp();
    $stamp->enableSsl('path', 'cert');
    $attributes = $this->getAttributesOfObject($stamp);
    $mqcd = $attributes['MQCD'];
    $this->assertEquals('TLS_RSA_WITH_AES_128_CBC_SHA256', $mqcd->getAttribute('SSLCipherSpec'));
    $this->assertEquals(MQSERIES_MQSCA_REQUIRED, $mqcd->getAttribute('SSLClientAuth'));
    $this->assertArrayHasKey('MQSCO', $attributes, 'The MQSCO structure was created.');
    $this->assertInstanceOf(MqscoStamp::class, $attributes['MQSCO']);
    $mqsco = $attributes['MQSCO'];
    $this->assertEquals('path', $mqsco->getAttribute('KeyRepository'));
    $this->assertEquals('cert', $mqsco->getAttribute('CertificateLabel'));
  }

  /**
   * Test disabling SSL.
   */
  public function testDisableSsl() {
    $stamp = new MqcnoStamp();
    $stamp->enableSsl('path', 'cert');
    $stamp->disableSsl();
    $attributes = $this->getAttributesOfObject($stamp);
    $mqcd = $attributes['MQCD'];
    $this->assertArrayNotHasKey('SSLCipherSpec', $mqcd->getAttributes());
    $this->assertArrayNotHasKey('SSLClientAuth', $mqcd->getAttributes());
    $this->assertArrayNotHasKey('MQSCO', $attributes);
  }

  /**
   * Test setting the connection name.
   */
  public function testSetConnectionName() {
    $stamp = new MqcnoStamp();
    $stamp->setConnectionName('host', 1100);
    $attributes = $this->getAttributesOfObject($stamp);
    $mqcd = $attributes['MQCD'];
    $this->assertEquals('host(1100)', $mqcd->getAttribute('ConnectionName'));
  }

  /**
   * Test setting authentication information.
   */
  public function testSetAuth() {
    $stamp = new MqcnoStamp();
    $stamp->setAuth('user', 'pass');
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayHasKey('MQCSP', $attributes, 'The MQCSP structure is created');
    $this->assertInstanceOf(MqcspStamp::class, $attributes['MQCSP']);
    $mqcsp = $attributes['MQCSP'];
    $this->assertEquals('user', $mqcsp->getAttribute('CSPUserId'));
    $this->assertEquals('pass', $mqcsp->getAttribute('CSPPassword'));
  }

  /**
   * Test unsetting authentication information.
   */
  public function testUnsetAuth() {
    $stamp = new MqcnoStamp();
    $stamp->setAuth('user', 'pass');
    $stamp->unsetAuth();
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayNotHasKey('MQCSP', $attributes);
  }

  /**
   * Test setting the SSL key repository.
   */
  public function testSetKeyRepository() {
    $stamp = new MqcnoStamp();
    $stamp->setKeyRepository('path', 'cert');
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayHasKey('MQSCO', $attributes);
    $mqsco = $attributes['MQSCO'];
    $this->assertInstanceOf(MqscoStamp::class, $mqsco);
    $this->assertEquals('path', $mqsco->getAttribute('KeyRepository'));
    $this->assertEquals('cert', $mqsco->getAttribute('CertificateLabel'));
  }

  /**
   * Test unsetting the SSL key repository.
   */
  public function testUnsetKeyRepository() {
    $stamp = new MqcnoStamp();
    $stamp->setKeyRepository('path', 'cert');
    $stamp->unsetKeyRepository();
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayNotHasKey('MQSCO', $attributes);
  }

  /**
   * Test setting the channel name.
   */
  public function testSetChannelName() {
    $stamp = new MqcnoStamp();
    $stamp->setChannelName('channel');
    $attributes = $this->getAttributesOfObject($stamp);
    $mqcd = $attributes['MQCD'];
    $this->assertEquals('channel', $mqcd->getAttribute('ChannelName'));
  }

  /**
   * Test getting an envelope from the stamp.
   */
  public function testToEnvelope() {
    $stamp = new MqcnoStamp();
    $envelope = $stamp->toEnvelope('queue_manager');
    $this->assertInstanceOf(MqcnoEnvelope::class, $envelope);
    $this->assertEquals('queue_manager', $envelope->getQueueManager());
  }

  /**
   * Test getting a connection from the stamp.
   */
  public function testToConnection() {
    // Mock a successful connection.
    global $_mqseries_comp_code;
    global $_mqseries_reason;
    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;

    $stamp = new MqcnoStamp();
    $connection = $stamp->toConnection('qm');
    $this->assertInstanceOf(MqConnection::class, $connection);
  }

}
