<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqscoStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQCSO Stamp.
 */
class MqcsoStampTest extends MqClientTestBase {

  /**
   * Test setting the SSL key repository.
   */
  public function testSetKeyRepository() {
    $stamp = new MqscoStamp();
    $stamp->setKeyRepository('path', 'cert');
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayHasKey('Version', $attributes);
    $this->assertGreaterThanOrEqual(5, $attributes['Version']);
    $this->assertArrayHasKey('KeyRepository', $attributes);
    $this->assertEquals('path', $attributes['KeyRepository']);
    $this->assertArrayHasKey('CertificateLabel', $attributes);
    $this->assertEquals('cert', $attributes['CertificateLabel']);
  }

  /**
   * Test unsetting the SSL key repository.
   */
  public function testUnsetkeyRepository() {
    $stamp = new MqscoStamp();
    $stamp->setKeyRepository('path', 'cert');
    $stamp->unsetKeyRepository();
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayNotHasKey('KeyRepository', $attributes);
    $this->assertArrayNotHasKey('CertificateLabel', $attributes);
  }

}
