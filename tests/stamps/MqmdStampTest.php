<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqmdStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQMD Stamp.
 */
class MqmdStampTest extends MqClientTestBase {

  /**
   * Constructor test.
   */
  public function testConstructor() {
    $stamp = new MqmdStamp();
    $this->assertGreaterThanOrEqual(2, $stamp->getAttribute('Version'));
  }

}
