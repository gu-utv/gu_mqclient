<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqpmoStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQPMO Stamp.
 */
class MqpmoStampTest extends MqClientTestBase {

  /**
   * Test enabling a sync point.
   */
  public function testEnableSyncPoint() {
    $stamp = new MqpmoStamp();
    $this->assertEquals(0, $stamp->getAttribute('Options') & MQSERIES_MQPMO_SYNCPOINT, 'Sync point is disabled by default.');
    $stamp->enableSyncPoint();
    $this->assertGreaterThan(0, $stamp->getAttribute('Options') & MQSERIES_MQPMO_SYNCPOINT, 'Sync point is enabled.');
  }

}
