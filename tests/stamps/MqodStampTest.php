<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqodStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the MQOD Stamp.
 */
class MqodStampTest extends MqClientTestBase {

  /**
   * Constructor tests.
   */
  public function testConstructor() {
    $stamp = new MqodStamp('object');
    $this->assertEquals('object', $stamp->getAttribute('ObjectName'));
  }

  /**
   * Test for setting the name of the object that should be open.
   */
  public function testSetObjectName() {
    $stamp = new MqodStamp('object');
    $this->assertNotEquals('name', $stamp->getAttribute('ObjectName'));
    $stamp->setObjectName('name');
    $this->assertEquals('name', $stamp->getAttribute('ObjectName'));
  }

}
