<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\MqStamp;
use gu\mqclient\tests\MqClientTestBase;

/**
 * Tests for the base stamp class.
 */
class MqStampTest extends MqClientTestBase {

  /**
   * Test getting an attribute value.
   *
   * @param string $key
   *   The attribute name.
   * @param mixed $value
   *   The attribute value.
   *
   * @dataProvider attributeProvider
   */
  public function testGetAttribute($key, $value) {
    $stamp = new MqStamp();
    $this->setAttributesOfObject($stamp, [$key => $value]);
    $attributes = $stamp->getAttributes();
    $this->assertArrayHasKey($key, $attributes);
    $this->assertEquals($value, $stamp->getAttribute($key));
  }

  /**
   * Test getting an attribute value.
   *
   * @param string $key
   *   The attribute name.
   * @param mixed $value
   *   The attribute value.
   *
   * @dataProvider attributeProvider
   */
  public function testSetAttribute($key, $value) {
    $stamp = new MqStamp();
    $stamp->setAttribute($key, $value);

    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayHasKey($key, $attributes);
    $this->assertEquals($value, $attributes[$key]);
  }

  /**
   * Test unsetting an attribute value.
   *
   * @param string $key
   *   The attribute name.
   * @param mixed $value
   *   The attribute value.
   *
   * @dataProvider attributeProvider
   */
  public function testUnsetAttribute($key, $value) {
    $stamp = new MqStamp();
    $stamp->setAttribute($key, $value);
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayHasKey($key, $attributes);
    $stamp->unsetAttribute($key);
    $attributes = $this->getAttributesOfObject($stamp);
    $this->assertArrayNotHasKey($key, $attributes);
  }

  /**
   * Test converting a stamp to an array.
   */
  public function testToArray() {
    $stamp = new MqStamp();
    $this->setAttributesOfObject($stamp, [
      'string' => 'value',
      'int' => 10,
      'array' => [
        'key' => 'value',
      ],
      'object' => new MqStamp(),
      'ignored_object' => new \stdClass(),
    ]);
    $array = $stamp->toArray();
    $this->assertEquals([
      'string' => 'value',
      'int' => 10,
      'array' => [
        'key' => 'value',
      ],
      'object' => [],
    ], $array);
  }

  /**
   * Test getting the attributes of a stamp.
   */
  public function testGetAttributes() {
    $array = [
      'string' => 'value',
      'int' => 10,
      'array' => [
        'key' => 'value',
      ],
    ];
    $stamp = new MqStamp();
    $this->setAttributesOfObject($stamp, $array);
    $this->assertEquals($array, $stamp->getAttributes());
  }

  /**
   * Data provider for attribute tests.
   *
   * @return array
   *   The provided data.
   */
  public function attributeProvider(): array {
    return [
      ['string', 'value'],
      ['int', 10],
      ['object', new MqStamp()],
      ['array', [
        'key' => 'value',
      ],
      ],
    ];
  }

}
