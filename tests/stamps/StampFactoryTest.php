<?php

namespace gu\mqclient\tests\stamps;

use gu\mqclient\stamps\StampFactory;
use gu\mqclient\stamps\StampInterface;
use PHPUnit\Framework\TestCase;

/**
 * Stamp Factory tests.
 */
class StampFactoryTest extends TestCase {

  /**
   * Test creating a generic stamp.
   */
  public function testCreate() {
    $factory = new StampFactory();
    $stamp = $factory->create();
    $this->assertInstanceOf(StampInterface::class, $stamp);
    $attributes = $stamp->toArray();
    $this->assertEmpty($attributes, 'Make sure we have no attributes');
  }

  /**
   * Test creating a connection stamp.
   */
  public function testCreateConnectionStamp() {
    $factory = new StampFactory();
    $stamp = $factory->createConnectionStamp('host', 1111, 'channel');
    $this->assertInstanceOf(StampInterface::class, $stamp);
    $this->assertGreaterThanOrEqual(5, $stamp->getAttribute('Version'), 'Stamp must be >= 5');
    $this->assertEquals(0, $stamp->getAttribute('Options'), 'Use standard binding by default');
    $mqcd = $stamp->getAttribute('MQCD');
    $this->assertInstanceOf(StampInterface::class, $mqcd, 'MQCD exists');
    $this->assertEquals('host(1111)', $mqcd->getAttribute('ConnectionName'), 'Connection name is set properly');
    $this->assertEquals('channel', $mqcd->getAttribute('ChannelName'), 'Channel name is set properly');
  }

  /**
   * Test creating an open object stamp.
   */
  public function testCreateOpenObjectStamp() {
    $factory = new StampFactory();
    $stamp = $factory->createOpenObjectStamp('queue');
    $this->assertInstanceOf(StampInterface::class, $stamp);
    $this->assertEquals('queue', $stamp->getAttribute('ObjectName'));
  }

}
