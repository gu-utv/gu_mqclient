<?php

namespace gu\mqclient\tests;

include_once 'MqseriesMockFunctions.php';

use gu\mqclient\envelopes\MqcnoEnvelope;
use gu\mqclient\envelopes\MqodEnvelope;
use gu\mqclient\MqConnection;
use gu\mqclient\MqException;
use gu\mqclient\stamps\MqodStamp;
use gu\mqclient\stamps\MqStamp;
use PHPUnit\Framework\TestCase;

/**
 * Test the MqConnection class.
 */
class MqConnectionTest extends TestCase {

  /**
   * Test a connection.
   *
   * @param int $comp_code
   *   Data for the connection's comp_code.
   * @param int $reason
   *   Data for the connection's reason code.
   *
   * @dataProvider connectProvider
   */
  public function testConnect(int $comp_code, int $reason) {
    // Mock a connection.
    global $_mqseries_comp_code;
    global $_mqseries_reason;
    $_mqseries_comp_code = $comp_code;
    $_mqseries_reason = $reason;

    $envelope = new MqcnoEnvelope(new MqStamp());
    $envelope->setQueueManager('qm');
    $connection = new MqConnection();
    if ($comp_code !== 0) {
      $this->expectException(MqException::class);
    }
    $connection->connect($envelope);
    if ($comp_code === 0) {
      $this->assertEquals($comp_code, $connection->lastCode());
      $this->assertEquals($reason, $connection->lastReason());
      $this->assertEquals($comp_code, $envelope->getAttribute('comp_code'));
      $this->assertEquals($reason, $envelope->getAttribute('reason'));
      // The mock function returns a number between 10 and 20.
      $this->greaterThanOrEqual(10, $envelope->getAttribute('connection'));
      $this->assertTrue($connection->isConnected());
    }
    else {
      $this->assertNotTrue($connection->isConnected());
    }
  }

  /**
   * Test disconneting a connection.
   *
   * @param int $comp_code
   *   Data for the connection's comp_code.
   * @param int $reason
   *   Data for the connection's reason code.
   *
   * @dataProvider connectProvider
   */
  public function testDisconnect(int $comp_code, int $reason) {
    // Mock a connection.
    global $_mqseries_comp_code;
    global $_mqseries_reason;

    $envelope = new MqcnoEnvelope(new MqStamp());
    $envelope->setQueueManager('qm');
    $connection = new MqConnection();

    // Mock a successful connection.
    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;
    $connection->connect($envelope);

    $_mqseries_comp_code = $comp_code;
    $_mqseries_reason = $reason;
    if ($comp_code !== 0) {
      $this->expectException(MqException::class);
    }
    $connection->disconnect($envelope);
    if ($comp_code === 0) {
      $this->assertEquals($comp_code, $connection->lastCode());
      $this->assertEquals($reason, $connection->lastReason());
      $this->assertEquals($comp_code, $envelope->getAttribute('comp_code'));
      $this->assertEquals($reason, $envelope->getAttribute('reason'));
      $this->assertFalse($connection->isConnected());
    }
  }

  /**
   * Test openning a queue.
   *
   * @param int $comp_code
   *   Data for the connection's comp_code.
   * @param int $reason
   *   Data for the connection's reason code.
   *
   * @dataProvider connectProvider
   */
  public function testOpen(int $comp_code, int $reason) {
    // Mock a connection.
    global $_mqseries_comp_code;
    global $_mqseries_reason;

    $envelope = new MqcnoEnvelope(new MqStamp());
    $envelope->setQueueManager('qm');
    $connection = new MqConnection();

    // Mock a successful connection.
    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;
    $connection->connect($envelope);

    $mqod = new MqodEnvelope(new MqodStamp('queue'), TRUE);

    $_mqseries_comp_code = $comp_code;
    $_mqseries_reason = $reason;
    if ($comp_code !== 0) {
      $this->expectException(MqException::class);
    }
    $connection->open($mqod);
    if ($comp_code === 0) {
      $this->assertEquals($comp_code, $connection->lastCode());
      $this->assertEquals($reason, $connection->lastReason());
      $this->assertEquals($comp_code, $mqod->getAttribute('comp_code'));
      $this->assertEquals($reason, $mqod->getAttribute('reason'));
      // The mock function returns a number between 10 and 20.
      $this->greaterThanOrEqual(10, $mqod->getOpenObject());
    }
  }

  /**
   * Test reading from a queue.
   *
   * @param int $comp_code
   *   Data for the connection's comp_code.
   * @param int $reason
   *   Data for the connection's reason code.
   *
   * @dataProvider connectProvider
   */
  public function testGet(int $comp_code, int $reason) {
    // Mock a connection.
    global $_mqseries_comp_code;
    global $_mqseries_reason;

    $envelope = new MqcnoEnvelope(new MqStamp());
    $envelope->setQueueManager('qm');
    $connection = new MqConnection();

    // Mock a successful connection.
    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;
    $connection->connect($envelope);

    $mqod = new MqodEnvelope(new MqodStamp('queue'), TRUE);

    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;
    $connection->open($mqod);

    $mqgmo = $mqod->toGetEnvelope(TRUE);
    $_mqseries_comp_code = $comp_code;
    $_mqseries_reason = $reason;

    if ($comp_code !== 0) {
      $this->expectException(MqException::class);
    }
    $connection->get($mqgmo);
    if ($comp_code === 0) {
      $this->assertEquals($comp_code, $connection->lastCode());
      $this->assertEquals($reason, $connection->lastReason());
      $this->assertEquals($comp_code, $mqgmo->getAttribute('comp_code'));
      $this->assertEquals($reason, $mqgmo->getAttribute('reason'));
      // The mock function returns a number between 10 and 20.
      $this->greaterThanOrEqual(10, (int) $mqgmo->getMessage());
    }
  }

  /**
   * Test comitting a read message to the queue.
   *
   * @param int $comp_code
   *   Data for the connection's comp_code.
   * @param int $reason
   *   Data for the connection's reason code.
   *
   * @dataProvider connectProvider
   */
  public function testCommit(int $comp_code, int $reason) {
    // .
    global $_mqseries_comp_code;
    global $_mqseries_reason;

    $envelope = new MqcnoEnvelope(new MqStamp());
    $envelope->setQueueManager('qm');
    $connection = new MqConnection();

    // Mock a successful MQ call.
    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;
    $connection->connect($envelope);
    $mqod = new MqodEnvelope(new MqodStamp('queue'), TRUE);
    $connection->open($mqod);
    $mqgmo = $mqod->toGetEnvelope(TRUE);
    $connection->get($mqgmo);

    $_mqseries_comp_code = $comp_code;
    $_mqseries_reason = $reason;
    if ($comp_code !== 0) {
      $this->expectException(MqException::class);
    }
    $connection->commit($mqgmo);
    if ($comp_code === 0) {
      $this->assertEquals($comp_code, $connection->lastCode());
      $this->assertEquals($reason, $connection->lastReason());
      $this->assertEquals($comp_code, $mqgmo->getAttribute('comp_code'));
      $this->assertEquals($reason, $mqgmo->getAttribute('reason'));
    }
  }

  /**
   * Test rolling back a read message to the queue.
   *
   * @param int $comp_code
   *   Data for the connection's comp_code.
   * @param int $reason
   *   Data for the connection's reason code.
   *
   * @dataProvider connectProvider
   */
  public function testRollback(int $comp_code, int $reason) {
    // .
    global $_mqseries_comp_code;
    global $_mqseries_reason;

    $envelope = new MqcnoEnvelope(new MqStamp());
    $envelope->setQueueManager('qm');
    $connection = new MqConnection();

    // Mock a successful MQ call.
    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;
    $connection->connect($envelope);
    $mqod = new MqodEnvelope(new MqodStamp('queue'), TRUE);
    $connection->open($mqod);
    $mqgmo = $mqod->toGetEnvelope(TRUE);
    $connection->get($mqgmo);

    $_mqseries_comp_code = $comp_code;
    $_mqseries_reason = $reason;
    if ($comp_code !== 0) {
      $this->expectException(MqException::class);
    }
    $connection->rollback($mqgmo);
    if ($comp_code === 0) {
      $this->assertEquals($comp_code, $connection->lastCode());
      $this->assertEquals($reason, $connection->lastReason());
      $this->assertEquals($comp_code, $mqgmo->getAttribute('comp_code'));
      $this->assertEquals($reason, $mqgmo->getAttribute('reason'));
    }
  }

  /**
   * Test putting a message in a queue.
   *
   * @param int $comp_code
   *   Data for the connection's comp_code.
   * @param int $reason
   *   Data for the connection's reason code.
   *
   * @dataProvider connectProvider
   */
  public function testPut(int $comp_code, int $reason) {
    // Mock a connection.
    global $_mqseries_comp_code;
    global $_mqseries_reason;

    $envelope = new MqcnoEnvelope(new MqStamp());
    $envelope->setQueueManager('qm');
    $connection = new MqConnection();

    // Mock a successful connection.
    $_mqseries_comp_code = 0;
    $_mqseries_reason = 0;
    $connection->connect($envelope);
    $mqod = new MqodEnvelope(new MqodStamp('queue'), TRUE);
    $connection->open($mqod);

    $mqpmo = $mqod->toPutEnvelope(TRUE);
    $_mqseries_comp_code = $comp_code;
    $_mqseries_reason = $reason;

    if ($comp_code !== 0) {
      $this->expectException(MqException::class);
    }
    $connection->put($mqpmo);
    if ($comp_code === 0) {
      $this->assertEquals($comp_code, $connection->lastCode());
      $this->assertEquals($reason, $connection->lastReason());
      $this->assertEquals($comp_code, $mqpmo->getAttribute('comp_code'));
      $this->assertEquals($reason, $mqpmo->getAttribute('reason'));
    }
  }

  /**
   * Data provider for all the tests.
   *
   * @return array
   *   The provided data.
   */
  public function connectProvider(): array {
    return [
      [0, 0],
      [1, 0],
      [1, 2033],
      [2, 0],
      [2, 2033],
    ];
  }

}
