<?php

/**
 * @file
 * Mocks for the mqseries functions in the namespace gu\mqclient.
 */

namespace gu\mqclient {

  /**
   * Generate mock comp_code and reason.
   *
   * @param int $comp_code
   *   The generated comp_code.
   * @param int $reason
   *   The generated reason.
   */
  function mqseries_mock_result(&$comp_code, &$reason) {
    global $_mqseries_comp_code;
    global $_mqseries_reason;
    if (isset($_mqseries_comp_code)) {
      $comp_code = $_mqseries_comp_code;
      $_mqseries_comp_code = 0;
    }
    else {
      $comp_code = 0;
    }
    if (isset($_mqseries_reason)) {
      $reason = $_mqseries_reason;
      $_mqseries_reason = 0;
    }
    else {
      $reason = 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  function mqseries_connx(string $queue_manager, array $mqcno, &$conn, &$comp_code, &$reason) {
    $conn = rand(10, 20);
    mqseries_mock_result($comp_code, $reason);
  }

  /**
   * {@inheritdoc}
   */
  function mqseries_disc($conn, &$comp_code, &$reason) {
    mqseries_mock_result($comp_code, $reason);
  }

  /**
   * {@inheritdoc}
   */
  function mqseries_open($conn, array $mqod, int $options, &$open_object, &$comp_code, &$reason) {
    $open_object = rand(10, 20);
    mqseries_mock_result($comp_code, $reason);
  }

  /**
   * {@inheritdoc}
   */
  function mqseries_get($conn, $object, $mqmd, $mqgmo, &$buffer_length, &$msg, &$data_length, &$comp_code, &$reason) {
    $msg = (string) rand(10, 20);
    $data_length = mb_strlen($msg);
    mqseries_mock_result($comp_code, $reason);
  }

  /**
   * {@inheritdoc}
   */
  function mqseries_cmit($conn, &$comp_code, &$reason) {
    mqseries_mock_result($comp_code, $reason);
  }

  /**
   * {@inheritdoc}
   */
  function mqseries_back($conn, &$comp_code, &$reason) {
    mqseries_mock_result($comp_code, $reason);
  }

  /**
   * {@inheritdoc}
   */
  function mqseries_put($conn, $object, $mqmd, $mqpmo, $msg, &$comp_code, &$reason) {
    mqseries_mock_result($comp_code, $reason);
  }

}
