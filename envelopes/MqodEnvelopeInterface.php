<?php

namespace gu\mqclient\envelopes;

/**
 * The object descriptor stamp.
 */
interface MqodEnvelopeInterface extends EnvelopeInterface {

  /**
   * Sets the options attribute.
   *
   * @param int $options
   *   The options attribute.
   */
  public function setOptions(int $options): void;

  /**
   * Gets the options attribute.
   *
   * @return int
   *   The value of the options attribute.
   */
  public function getOptions(): int;

  /**
   * Sets the open object.
   *
   * @param resource $open_object
   *   The open object.
   */
  public function setOpenObject($open_object);

  /**
   * Gets the open object.
   *
   * @return resource
   *   The open object.
   */
  public function getOpenObject();

  /**
   * Creates a put message options envelope for the open object.
   *
   * @return MqpmoEnvelopeInterface
   *   A MqpmoEnvelope for the open object.
   */
  public function toPutEnvelope(bool $sync_point): MqpmoEnvelopeInterface;

  /**
   * Creates a get/put message options envelope for the open object.
   *
   * @return MqgmoEnvelopeInterface
   *   A MqgmoEnvelope for the open object.
   */
  public function toGetEnvelope(bool $sync_point, int $max_message_size = MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT): MqgmoEnvelopeInterface;

  /**
   * Return the queue name of this envelope.
   *
   * @return string|null
   *   The queue name.
   */
  public function getQueueName() : ?string;

}
