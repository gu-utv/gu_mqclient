<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * The get message envelope.
 */
interface MqgmoEnvelopeInterface extends MqQueueMessageEnvelopeInterface {

  /**
   * Create a message envelope.
   *
   * @param \gu\mqclient\stamps\StampInterface $mqgmo
   *   The get message stamp.
   * @param \gu\mqclient\stamps\StampInterface $mqmd
   *   The message descriptor stamp.
   */
  public function __construct(StampInterface $mqgmo, StampInterface $mqmd);

}
