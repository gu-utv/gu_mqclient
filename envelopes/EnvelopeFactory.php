<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * Creates envelopes.
 */
class EnvelopeFactory implements EnvelopeFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create(StampInterface $stamp): EnvelopeInterface {
    return new MqEnvelope($stamp);
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenObjectEnvelope(StampInterface $stamp, bool $readonly = FALSE): MqodEnvelopeInterface {
    return new MqodEnvelope($stamp, $readonly);
  }

  /**
   * {@inheritdoc}
   */
  public function createConnectionEnvelope(StampInterface $stamp, string $queue_manager): MqcnoEnvelopeInterface {
    $envelope = new MqcnoEnvelope($stamp);
    $envelope->setQueueManager($queue_manager);
    return $envelope;
  }

}
