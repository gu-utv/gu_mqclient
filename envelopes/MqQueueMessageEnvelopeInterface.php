<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * The get/put message options envelope interface.
 */
interface MqQueueMessageEnvelopeInterface extends EnvelopeInterface {

  /**
   * The default maximum message size.
   */
  public const MAX_MESSAGE_SIZE_DEFAULT = 104857600;

  /**
   * Changes the message descriptor stamp.
   *
   * @param \gu\mqclient\stamps\StampInterface $mqmd
   *   A new message descriptor.
   */
  public function setMessageDescriptor(StampInterface $mqmd): void;

  /**
   * Returns the message descriptor stamp.
   *
   * @return \gu\mqclient\stamps\StampInterface
   *   The message descriptor stamp.
   */
  public function getMessageDescriptor(): StampInterface;

  /**
   * Sets the queue (open object) for the envelope.
   *
   * @param resource $queue_handle
   *   The handle of an open object.
   *
   * @see MqodEnvelopeInterface
   */
  public function setQueue($queue_handle): void;

  /**
   * Returns the queue (open object)
   *
   * @return resource
   *   The open object, or null.
   */
  public function getQueue();

  /**
   * Gets the message stored in the envelope.
   *
   * @return object|resource|array|string|int|float|bool|null
   *   The message, normally a C structure casted in a string buffer.
   */
  public function getMessage();

  /**
   * Sets the message of the envelope.
   *
   * @param mixed $message
   *   The message of the envelope.
   */
  public function setMessage($message): void;

}
