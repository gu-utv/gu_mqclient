<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\MqgmoStamp;
use gu\mqclient\stamps\MqmdStamp;
use gu\mqclient\stamps\MqodStamp;
use gu\mqclient\stamps\MqpmoStamp;
use gu\mqclient\stamps\StampInterface;

/**
 * The object descriptor envelope.
 */
class MqodEnvelope extends MqEnvelope implements MqodEnvelopeInterface {

  /**
   * {@inheritdoc}
   */
  protected $attributes = [
    'stamps' => [],
    'options' => \MQSERIES_MQOO_FAIL_IF_QUIESCING,
  ];

  /**
   * Create the MQOD envelope object.
   *
   * @param \gu\mqclient\stamps\StampInterface $stamp
   *   The first stamp.
   * @param bool $readonly
   *   Whether to open as read only.
   */
  public function __construct(StampInterface $stamp, bool $readonly = FALSE) {
    parent::__construct($stamp);
    if ($readonly) {
      $this->setOptions($this->getOptions() | \MQSERIES_MQOO_INPUT_SHARED);
    }
    else {
      $this->setOptions($this->getOptions() | \MQSERIES_MQOO_OUTPUT);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(int $value): void {
    $this->setAttribute('options', $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(): int {
    return $this->getAttribute('options');
  }

  /**
   * {@inheritdoc}
   */
  public function setOpenObject($open_object) {
    $this->setAttribute('open_object', $open_object);
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenObject() {
    return $this->getAttribute('open_object');
  }

  /**
   * {@inheritdoc}
   */
  public function toGetEnvelope(bool $sync_point, int $max_message_size = MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT): MqgmoEnvelopeInterface {
    $mqgmo_stamp = new MqgmoStamp();
    if ($sync_point) {
      $mqgmo_stamp->enableSyncPoint();
    }
    $envelope = new MqgmoEnvelope($mqgmo_stamp, new MqmdStamp());
    $envelope->setQueue($this->getOpenObject());
    $envelope->setMaxMessageSize($max_message_size);
    return $envelope;
  }

  /**
   * {@inheritdoc}
   */
  public function toPutEnvelope(bool $sync_point): MqpmoEnvelopeInterface {
    $mqpmo_stamp = new MqpmoStamp();
    if ($sync_point) {
      $mqpmo_stamp->enableSyncPoint();
    }
    $envelope = new MqpmoEnvelope($mqpmo_stamp, new MqmdStamp());
    $envelope->setQueue($this->getOpenObject());
    return $envelope;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueueName(): ?string {
    if ($this->getFirstStamp() instanceof MqodStamp) {
      return $this->getFirstStamp()->getObjectName();
    }
    return NULL;
  }

}
