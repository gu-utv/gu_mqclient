<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * The put message options envelope interface.
 */
interface MqpmoEnvelopeInterface extends MqQueueMessageEnvelopeInterface {

  /**
   * Creates a put message options envelope.
   *
   * @param \gu\mqclient\stamps\StampInterface $mqpmo
   *   The put message options stamp.
   * @param \gu\mqclient\stamps\StampInterface $mqmd
   *   The message descriptor stamp.
   */
  public function __construct(StampInterface $mqpmo, StampInterface $mqmd);

}
