<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * A MQGMO envelope.
 */
class MqgmoEnvelope extends MqQueueMessageEnvelope implements MqgmoEnvelopeInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(StampInterface $mqgmo, StampInterface $mqmd) {
    parent::__construct($mqgmo);
    $this->setMessageDescriptor($mqmd);
  }

}
