<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * The base MqEnvelope class.
 */
class MqEnvelope implements EnvelopeInterface {

  /**
   * The attribute storage array.
   *
   * @var array
   */
  protected $attributes = [
    'stamps' => [],
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(?StampInterface $stamp = NULL) {
    if ($stamp instanceof StampInterface) {
      $this->attributes['stamps'] = [$stamp];
    }
    else {
      $this->attributes['stamps'] = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addStamp(StampInterface $stamp): void {
    $this->attributes['stamps'][] = $stamp;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstStamp(): StampInterface {
    return reset($this->attributes['stamps']);
  }

  /**
   * {@inheritdoc}
   */
  public function getStamp(string $type): StampInterface {
    foreach ($this->attributes['stamps'] as $stamp) {
      if (get_class($stamp) == $type) {
        return $stamp;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttribute($name) {
    return empty($this->attributes[$name]) ? NULL : $this->attributes[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function setAttribute($name, $value): void {
    $this->attributes[$name] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCallResults(int $comp_code, int $reason): void {
    $this->setAttribute('comp_code', $comp_code);
    $this->setAttribute('reason', $reason);
  }

}
