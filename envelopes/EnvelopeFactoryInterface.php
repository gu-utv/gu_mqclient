<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * The envelope factory interface.
 */
interface EnvelopeFactoryInterface {

  /**
   * Creates a new envelope.
   *
   * @param \gu\mqclient\stamps\StampInterface $stamp
   *   A stamp.
   *
   * @return \GU\mqclient\envelopes\EnvelopeInterface
   *   A new envelope;
   */
  public function create(StampInterface $stamp): EnvelopeInterface;

  /**
   * Creates a new MQCNO envelope for a connection.
   *
   * @param \gu\mqclient\stamps\StampInterface $stamp
   *   A MQCNO Stamp.
   * @param string $queue_manager
   *   A queue manager name.
   *
   * @return MqcnoEnvelopeInterface
   *   The new MQCNO envelope.
   */
  public function createConnectionEnvelope(StampInterface $stamp, string $queue_manager): MqcnoEnvelopeInterface;

  /**
   * Create a new open object envelope.
   *
   * @param \gu\mqclient\stamps\StampInterface $stamp
   *   A stamp.
   * @param bool $readonly
   *   Open as readonly (GET only).
   *
   * @return \GU\mqclient\envelopes\MqodEnvelopeInterface
   *   The new open object envelope.
   */
  public function createOpenObjectEnvelope(StampInterface $stamp, bool $readonly = FALSE): MqodEnvelopeInterface;

}
