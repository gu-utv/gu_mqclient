<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * The Get/Put message envelope.
 */
class MqQueueMessageEnvelope extends MqEnvelope implements MqQueueMessageEnvelopeInterface {

  /**
   * {@inheritdoc}
   */
  protected $attributes = [
    'stamps' => [],
    'mqmd' => NULL,
    'max_message_size' => MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT,
  ];

  /**
   * {@inheritdoc}
   */
  public function setMessageDescriptor(StampInterface $mqmd): void {
    $this->setAttribute('mqmd', $mqmd);
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageDescriptor(): StampInterface {
    return $this->getAttribute('mqmd');
  }

  /**
   * {@inheritdoc}
   */
  public function setMaxMessageSize(int $max_message_size) {
    $this->setAttribute('max_message_size', $max_message_size);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxMessageSize(): int {
    return $this->getAttribute('max_message_size');
  }

  /**
   * {@inheritdoc}
   */
  public function setQueue($queue_handle): void {
    $this->setAttribute('open_object', $queue_handle);
  }

  /**
   * {@inheritdoc}
   */
  public function getQueue() {
    return $this->getAttribute('open_object');
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->getAttribute('message');
  }

  /**
   * {@inheritdoc}
   */
  public function setMessage($message, int $data_length = NULL): void {
    $this->setAttribute('message', $message);

    if (!empty($data_length)) {
      $this->setAttribute('data_length', $data_length);
    }
  }

}
