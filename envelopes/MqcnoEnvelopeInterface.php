<?php

namespace gu\mqclient\envelopes;

/**
 * The Connection envelope interface.
 */
interface MqcnoEnvelopeInterface extends EnvelopeInterface {

  /**
   * Returns the queue manager handle of the connection.
   *
   * @return string
   *   The name of the queue manager.
   */
  public function getQueueManager(): string;

  /**
   * Sets the queue manager name.
   *
   * @param string $queue_manager
   *   The queue manager name.
   */
  public function setQueueManager(string $queue_manager): void;

}
