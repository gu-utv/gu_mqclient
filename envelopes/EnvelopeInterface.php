<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * The envelope interface.
 */
interface EnvelopeInterface {

  /**
   * Add a stamp to the envelope.
   *
   * @param \gu\mqclient\stamps\StampInterface $stamp
   *   The stamp to add.
   */
  public function addStamp(StampInterface $stamp): void;

  /**
   * Get a stamp from the envelope by its type name.
   *
   * @param string $type
   *   The type name.
   *
   * @return \gu\mqclient\stamps\StampInterface
   *   A stamp that matches the requested type.
   */
  public function getStamp(string $type): StampInterface;

  /**
   * Gets a specific stamp on the envelope.
   *
   * @return \gu\mqclient\stamps\StampInterface
   *   The first stamp on the envelope.
   */
  public function getFirstStamp(): StampInterface;

  /**
   * Gets the attribute value.
   *
   * @param string $name
   *   The attribute name.
   *
   * @return mixed
   *   The attribute value.
   */
  public function getAttribute($name);

  /**
   * Sets an attribute value.
   *
   * @param string $attribute
   *   The attribute name.
   * @param mixed $value
   *   The value of the attribute.
   */
  public function setAttribute(string $attribute, $value): void;

  /**
   * Stores the MQ Call results.
   *
   * @param int $comp_code
   *   The result code from the MQ call.
   * @param int $reason
   *   The result reason from the MQ call.
   */
  public function setCallResults(int $comp_code, int $reason): void;

}
