<?php

namespace gu\mqclient\envelopes;

/**
 * The connection envelope.
 */
class MqcnoEnvelope extends MqEnvelope implements MqcnoEnvelopeInterface {

  /**
   * {@inheritdoc}
   */
  public function getQueueManager(): string {
    return $this->attributes['queue_manager'];
  }

  /**
   * {@inheritdoc}
   */
  public function setQueueManager(string $queue_manager): void {
    $this->setAttribute('queue_manager', $queue_manager);
  }

}
