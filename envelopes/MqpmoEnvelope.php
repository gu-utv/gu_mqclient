<?php

namespace gu\mqclient\envelopes;

use gu\mqclient\stamps\StampInterface;

/**
 * An MQPMO envelope.
 */
class MqpmoEnvelope extends MqQueueMessageEnvelope implements MqpmoEnvelopeInterface {

  /**
   * {@inheritdoc}
   */
  protected $attributes = [
    'stamps' => [],
    'mqmd' => NULL,
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(StampInterface $mqpmo, StampInterface $mqmd) {
    parent::__construct($mqpmo);
    $this->setMessageDescriptor($mqmd);
  }

}
