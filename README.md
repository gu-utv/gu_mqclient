# MQ Client #

## Application setup ##
An application can require mqclient via composer.
The client requires information about the MQ server it should communicate with, the code examples assume the configuration is passed in an array like:
``
  $config = [
    'host' => '',
    'port' => '',
    'qmanager' => '',
    'channel' => '',
    'queue' => '',
    'user' => '',
    'pass' => '',
    'key_repo' => '',
  ];
``
## Connecting to a queue manager ##
``
    $mqcno_stamp = new MqcnoStamp($config['host'], $config['port'], $config['channel']);
    $mqcno_stamp->enableSSL($config['key_repo']);
    $mqcno_stamp->setAuth($config['user'], $config['pass']);

    $conn = $mqcno_stamp->toConnection($config['qmanager']);
``
## Opening a queue for reading or writing ##
``
    $mqod = new MqodEnvelope(new MqodStamp($config['queue']));
    $conn->open($mqod);
``
The envelope $mqod will contain a reference to an open object (normally a queue).
## Sending messages ##
``
    $mqpmo = $mqod->toPutEnvelope(true);
    $mqpmo->setMessage('Message 1');
    $conn->put($mqpmo);
    $mqpmo->setMessage('Message 2');
    $conn->put($mqpmo);
    $conn->commit($mqpmo);
``
## Reading messages ##
``
    $mqgmo = $mqod->toGetEnvelope(true);
    $conn->get($mqgmo);
    $msg = $mqgmo->getMessage();
    $conn->commit($mqgmo);
``
## Disconnecting ##
``
    $conn->disconnect($mqpmo);
``
## Handling errors ##
The connection object throws an MqException when there are errors, for example, when there are configuration errors or when the server can not be reached. Some MQ calls return a reason code, and it might be useful to check it after each call.
``
    $conn->lastReason();
    $conn->lastReasonString();
``
For example, `$conn->get($mqgmo)` calls can return reason `2033` when there are no more messages in the queue. The string for this reason is `No message available.`