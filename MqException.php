<?php

namespace gu\mqclient;

/**
 * An MQ Exception class.
 */
class MqException extends \Exception {
}
