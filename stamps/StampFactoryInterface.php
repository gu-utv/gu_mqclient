<?php

namespace gu\mqclient\stamps;

/**
 * Undocumented interface.
 */
interface StampFactoryInterface {

  /**
   * Creates a new generic stamp.
   *
   * @return StampInterface
   *   A new stamp;
   */
  public static function create(): StampInterface;

  /**
   * Creates a new Mqcno Stamp.
   *
   * @param string $host
   *   The host name.
   * @param int $port
   *   The port number.
   * @param string $channel_name
   *   The channel name.
   *
   * @return StampInterface
   *   A new Mqcno stamp.
   */
  public function createConnectionStamp(string $host, int $port, string $channel_name): StampInterface;

  /**
   * Creates a new MQOD stamp.
   *
   * @param string $queue_name
   *   The queue (or an object) name.
   *
   * @return StampInterface
   *   The returned stamp.
   */
  public function createOpenObjectStamp(string $queue_name): StampInterface;

}
