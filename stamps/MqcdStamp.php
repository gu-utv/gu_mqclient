<?php

namespace gu\mqclient\stamps;

/**
 * The channel definition Structure (MQCD).
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=structures-mqcd-channel-definition
 */
class MqcdStamp extends MqStamp {

  /**
   * {@inheritdoc}
   */
  protected $attributes = [
  // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=fields-version-mqlong
    'Version' => 7,
  // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=fields-transporttype-mqlong
    'TransportType' => \MQSERIES_MQXPT_TCP,
  // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=fields-maxmsglength-mqlong
    'MaxMsgLength' => 1024 * 1024 * 100,
  ];

  /**
   * Enables SSL connection to the channel.
   */
  public function enableSsl(): void {
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=fields-sslcipherspec-mqchar32
    $this->setAttribute('SSLCipherSpec', 'TLS_RSA_WITH_AES_128_CBC_SHA256');
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=fields-sslclientauth-mqlong
    $this->setAttribute('SSLClientAuth', \MQSERIES_MQSCA_REQUIRED);
  }

  /**
   * Disables SSL connection to the channel.
   */
  public function disableSsl() {
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=fields-sslcipherspec-mqchar32
    $this->unsetAttribute('SSLCipherSpec');
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=fields-sslclientauth-mqlong
    $this->unsetAttribute('SSLClientAuth');
  }

  /**
   * Sets the connection name according to MQ's format.
   *
   * @param string $host
   *   The host name of the server hosting the channel.
   * @param int $port
   *   The port number of the channel.
   *
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=order-connection-name-conname
   */
  public function setConnectionName(string $host, int $port): void {
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=order-connection-name-conname
    $this->setAttribute('ConnectionName', "$host($port)");
  }

  /**
   * Sets the channel name of the stamp.
   *
   * @param string $channel_name
   *   The channel name.
   */
  public function setChannelName(string $channel_name): void {
    $this->setAttribute('ChannelName', $channel_name);
  }

}
