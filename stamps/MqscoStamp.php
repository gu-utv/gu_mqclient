<?php

namespace gu\mqclient\stamps;

/**
 * The SSL/TLS security configuration structure MQSCO.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqi-mqsco-ssltls-configuration-options
 */
class MqscoStamp extends MqStamp {

  /**
   * Sets the key repository path according to MQ's format.
   *
   * @param string $path
   *   The path of the key repository in MQ's format.
   * @param string $cert_label
   *   The label of the client certificate in the key repository.
   *
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqsco-keyrepository-mqchar256
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqsco-certificatelabel-mqchar64
   */
  public function setKeyRepository(string $path, string $cert_label = ''): void {
    $this->setAttribute('Version', 5);
    $this->setAttribute('KeyRepository', $path);
    $this->setAttribute('CertificateLabel', $cert_label);
  }

  /**
   * Unsets SSL/TLS configuration.
   */
  public function unsetKeyRepository(): void {
    $this->unsetAttribute('KeyRepository');
    $this->unsetAttribute('CertificateLabel');
  }

}
