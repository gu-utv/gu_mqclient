<?php

namespace gu\mqclient\stamps;

use gu\mqclient\envelopes\MqcnoEnvelope;
use gu\mqclient\envelopes\MqcnoEnvelopeInterface;
use gu\mqclient\MqConnection;

/**
 * The connect option structure MQCNO.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqi-mqcno-connect-options
 */
class MqcnoStamp extends MqStamp implements MqcnoStampInterface {

  /**
   * {@inheritdoc}
   */
  protected $attributes = [
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcno-version-mqlong
    'Version' => 5,
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct();
    // @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcno-clientconnptr-mqptr
    $this->attributes['MQCD'] = new MqcdStamp();
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcno-options-mqlong
    $this->attributes['Options'] = defined("MQSERIES_MQCNO_STANDARD_BINDING") ? \MQSERIES_MQCNO_STANDARD_BINDING : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function enableSsl(string $path, $cert_label = ''): void {
    $this->attributes['MQCD']->enableSsl();
    $this->setKeyRepository($path, $cert_label);
  }

  /**
   * {@inheritdoc}
   */
  public function disableSsl(): void {
    $this->attributes['MQCD']->disableSsl();
    $this->unsetKeyRepository();
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectionName(string $host, int $port): void {
    $this->attributes['MQCD']->setConnectionName($host, $port);
  }

  /**
   * {@inheritdoc}
   */
  public function setAuth(string $username, string $password): void {
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcno-securityparmsptr-pmqcsp
    $this->setAttribute('MQCSP', new MqcspStamp());
    $this->attributes['MQCSP']->setAuth($username, $password);
  }

  /**
   * {@inheritdoc}
   */
  public function unsetAuth(): void {
    $this->unsetAttribute('MQCSP');
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyRepository(string $path, string $cert_label = ''): void {
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcno-sslconfigptr-pmqsco
    $this->setAttribute('MQSCO', new MqscoStamp());
    $this->attributes['MQSCO']->setKeyRepository($path, $cert_label);
  }

  /**
   * {@inheritdoc}
   */
  public function unsetKeyRepository(): void {
    unset($this->attributes['MQSCO']);
  }

  /**
   * {@inheritdoc}
   */
  public function setChannelName(string $channel_name): void {
    $this->attributes['MQCD']->setChannelName($channel_name);
  }

  /**
   * {@inheritdoc}
   */
  public function toEnvelope(string $queue_manager): MqcnoEnvelopeInterface {
    $envelope = new MqcnoEnvelope($this);
    $envelope->setQueueManager($queue_manager);
    return $envelope;
  }

  /**
   * {@inheritdoc}
   */
  public function toConnection(string $queue_manager): MqConnection {
    $conn = new MqConnection();
    $envelope = $this->toEnvelope($queue_manager);
    $conn->connect($envelope);
    return $conn;
  }

}
