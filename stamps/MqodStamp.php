<?php

namespace gu\mqclient\stamps;

/**
 * The Object descriptor structure MQOD.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqi-mqod-object-descriptor
 */
class MqodStamp extends MqStamp {

  /**
   * Creates an object descriptor stamp.
   *
   * @param string $object_name
   *   The object name.
   */
  public function __construct(string $object_name) {
    $this->setObjectName($object_name);
  }

  /**
   * Set the object name (normall a queue name) to open.
   *
   * @param string $object_name
   *   The object name.
   */
  public function setObjectName(string $object_name): void {
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqod-objectname-mqchar48
    $this->setAttribute('ObjectName', $object_name);
  }

  /**
   * Returns the object name.
   *
   * @return string
   *   The object name.
   */
  public function getObjectName() : string {
    return $this->getAttribute('ObjectName');
  }

}
