<?php

namespace gu\mqclient\stamps;

/**
 * The MQCSP structure.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqi-mqcsp-security-parameters
 */
class MqcspStamp extends MqStamp {

  /**
   * Enables user/pass authentication in the connection.
   *
   * @param string $username
   *   The username.
   * @param string $password
   *   The password in plain text.
   */
  public function setAuth(string $username, string $password): void {
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcsp-cspuseridptr-mqptr
    $this->setAttribute('CSPUserId', $username);
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcsp-cspuseridptr-mqptr
    $this->setAttribute('CSPPassword', $password);
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcsp-authenticationtype-mqlong
    $this->setAttribute('AuthenticationType', \MQSERIES_MQCSP_AUTH_USER_ID_AND_PWD);
  }

  /**
   * Disables user/pass authentication in the connection.
   */
  public function unsetAuth(): void {
    $this->unsetAttribute('CSPUserId');
    $this->unsetAttribute('CSPPassword');
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqcsp-authenticationtype-mqlong
    $this->setAttribute('AuthenticationType', \MQSERIES_MQCSP_AUTH_NONE);
  }

}
