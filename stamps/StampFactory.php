<?php

namespace gu\mqclient\stamps;

/**
 * Undocumented interface.
 */
class StampFactory implements StampFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(): StampInterface {
    return new MqStamp();
  }

  /**
   * {@inheritdoc}
   */
  public function createConnectionStamp(string $host, int $port, string $channel_name): StampInterface {
    $stamp = new MqcnoStamp();
    $stamp->setConnectionName($host, $port);
    $stamp->setChannelName($channel_name);
    return $stamp;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenObjectStamp(string $queue_name): StampInterface {
    return new MqodStamp($queue_name);
  }

}
