<?php

namespace gu\mqclient\stamps;

/**
 * The MQ base stamp.
 */
class MqStamp implements StampInterface {
  /**
   * The attribute storage.
   *
   * @var array
   */
  protected $attributes = [];

  /**
   * Dummy constructor.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  public function getAttribute(string $attribute) {
    return $this->attributes[$attribute];
  }

  /**
   * {@inheritdoc}
   */
  public function setAttribute(string $attribute, $value): void {
    $this->attributes[$attribute] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetAttribute(string $attribute): void {
    unset($this->attributes[$attribute]);
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    $ret = [];
    foreach ($this->attributes as $key => $value) {
      if (is_object($value) && method_exists($value, 'toArray')) {
        $ret[$key] = $value->toArray();
      }
      elseif (is_numeric($value) || is_string($value) || is_array($value)) {
        $ret[$key] = $value;
      }
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttributes(): array {
    return $this->attributes;
  }

}
