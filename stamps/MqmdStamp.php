<?php

namespace gu\mqclient\stamps;

/**
 * The message descriptor structure MQMD.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqi-mqmd-message-descriptor
 */
class MqmdStamp extends MqStamp {
  /**
   * {@inheritdoc}
   */
  protected $attributes = [
    'Version' => MQSERIES_MQMD_VERSION_2,
  ];

}
