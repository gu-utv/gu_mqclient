<?php

namespace gu\mqclient\stamps;

/**
 * The Stamp interface.
 */
interface StampInterface {

  /**
   * Gets the attribute value.
   *
   * @param string $attribute
   *   The attribute name.
   *
   * @return object|resource|array|string|int|float|bool|null
   *   The attribute value.
   */
  public function getAttribute(string $attribute);

  /**
   * Sets an attribute value.
   *
   * @param string $attribute
   *   The attribute name.
   * @param object|resource|array|string|int|float|bool|null $value
   *   The value of the attribute.
   */
  public function setAttribute(string $attribute, $value): void;

  /**
   * Unset an attribute.
   *
   * @param string $attribute
   *   The attribute name.
   */
  public function unsetAttribute(string $attribute): void;

  /**
   * Return the attributes as an array.
   *
   * @return array
   *   An array representing the stamp.
   */
  public function toArray(): array;

  /**
   * Get the attributes array.
   *
   * @return array
   *   The attributes array.
   */
  public function getAttributes(): array;

}
