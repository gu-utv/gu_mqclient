<?php

namespace gu\mqclient\stamps;

use gu\mqclient\envelopes\MqcnoEnvelopeInterface;
use gu\mqclient\MqConnection;

/**
 * The connect option structure MQCNO.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqi-mqcno-connect-options
 */
interface MqcnoStampInterface extends StampInterface {

  /**
   * Enable SSL in the connection.
   *
   * @param string $path
   *   The path of the certificate reporitory in a special format.
   * @param string $cert_label
   *   The certificate label to use in two-way SSL connections.
   *
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqsco-keyrepository-mqchar256
   */
  public function enableSsl(string $path, $cert_label = ''): void;

  /**
   * Disable SSL in the connection.
   */
  public function disableSsl(): void;

  /**
   * Set the connection name in MQ's special format.
   *
   * @param string $host
   *   The server's host name.
   * @param int $port
   *   The server's IP address.
   *
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=order-connection-name-conname
   */
  public function setConnectionName(string $host, int $port): void;

  /**
   * Prepares the connection for user/pass authetication.
   *
   * @param string $username
   *   The user name.
   * @param string $password
   *   The password.
   */
  public function setAuth(string $username, string $password): void;

  /**
   * Disables user/pass authentication if enabled.
   */
  public function unsetAuth(): void;

  /**
   * Configures the key repository for SSL Connections.
   *
   * @param string $path
   *   The path of the key repository in MQ's special format.
   * @param string $cert_label
   *   The certificate label.
   *
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqsco-keyrepository-mqchar256
   */
  public function setKeyRepository(string $path, string $cert_label = ''): void;

  /**
   * Removes SSL Key repository configuration.
   */
  public function unsetKeyRepository(): void;

  /**
   * Sets the channel name on this envelope.
   *
   * @param string $channel_name
   *   The channel name.
   */
  public function setChannelName(string $channel_name): void;

  /**
   * Get an MqcnoEnvelope from this stamp.
   *
   * @param string $queue_manager
   *   The name of a queue manager.
   *
   * @return \GU\mqclient\envelopes\MqcnoEnvelopeInterface
   *   An MQCNO envelope.
   */
  public function toEnvelope(string $queue_manager): MqcnoEnvelopeInterface;

  /**
   * Get a connected connection from this envelope.
   *
   * @param string $queue_manager
   *   The name of a queue manager.
   *
   * @return \GU\mqclient\MqConnection
   *   A connected connection.
   */
  public function toConnection(string $queue_manager): MqConnection;

}
