<?php

namespace gu\mqclient\stamps;

/**
 * The MQ Get Message Option Structure MQGMO.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqgmo-options-mqlong
 */
class MqgmoStamp extends MqStamp {

  /**
   * {@inheritdoc}
   */
  protected $attributes = [
    'Options' => MQSERIES_MQGMO_FAIL_IF_QUIESCING,
  ];

  /**
   * Sets a sync point with the get request, which starts a transaction.
   *
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqgmo-options-mqlong
   */
  public function enableSyncPoint() {
    $this->setAttribute('Options', $this->getAttribute('Options') | \MQSERIES_MQGMO_SYNCPOINT);
  }

  /**
   * Enables waiting for a message if no one is available.
   *
   * @param int $interval
   *   The wait interval in milliseconds.
   *
   * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqgmo-options-mqlong
   */
  public function enableWait(int $interval): void {
    $this->setAttribute('Options', $this->getAttribute('Options') | \MQSERIES_MQGMO_WAIT);
    // https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqgmo-waitinterval-mqlong
    $this->setAttribute('WaitInterval', $interval);
  }

}
