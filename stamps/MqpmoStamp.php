<?php

namespace gu\mqclient\stamps;

/**
 * The MQ put message options structure MQPMO.
 *
 * @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqgmo-options-mqlong
 */
class MqpmoStamp extends MqStamp {

  /**
   * {@inheritdoc}
   */
  protected $attributes = [
    'Options' => MQSERIES_MQGMO_FAIL_IF_QUIESCING,
  ];

  /**
   * Sets a syncpoint, which enables transactions.
   */
  public function enableSyncPoint(): void {
    // @see https://www.ibm.com/docs/en/ibm-mq/9.2?topic=mqpmo-options-mqlong
    $this->setAttribute('Options', $this->getAttribute('Options') | \MQSERIES_MQPMO_SYNCPOINT);
  }

}
