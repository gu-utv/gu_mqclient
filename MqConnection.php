<?php

namespace gu\mqclient;

use gu\mqclient\envelopes\EnvelopeInterface;
use gu\mqclient\envelopes\MqcnoEnvelopeInterface;
use gu\mqclient\envelopes\MqgmoEnvelopeInterface;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\envelopes\MqpmoEnvelopeInterface;

/**
 * The MqConnection class.
 */
class MqConnection {

  /**
   * The connection.
   *
   * @var resource
   */
  protected $conn;

  /**
   * The last completion code.
   *
   * @var int
   */
  protected $compCode;

  /**
   * The last reason.
   *
   * @var int
   */
  protected $reason;

  /**
   * Whether the connection is active.
   *
   * @var bool
   */
  protected $connected = FALSE;

  /**
   * A reference to the last open object.
   *
   * @var resource
   */
  protected $openObject;

  /**
   * Connect to an IBM MQ server.
   *
   * @param \GU\mqclient\envelopes\MqcnoEnvelopeInterface $envelope
   *   The MQCNO envelope.
   */
  public function connect(MqcnoEnvelopeInterface $envelope) {
    if ($this->connected) {
      throw new MqException("Already Connected, will not connect again to avoid side issues");
    }
    $mqcno = $envelope->getFirstStamp()->toArray();
    $conn = &$this->conn;
    $comp_code = &$this->compCode;
    $reason = &$this->reason;
    mqseries_connx($envelope->getQueueManager(), $mqcno, $conn, $comp_code, $reason);
    $this->processCallResult();
    $envelope->setCallResults($this->compCode, $this->reason);
    $envelope->setAttribute('connection', $this->conn);
    $this->connected = TRUE;
  }

  /**
   * Connect to an IBM MQ server.
   *
   * @param \GU\mqclient\envelopes\EnvelopeInterface $envelope
   *   The MQCNO envelope.
   */
  public function disconnect(EnvelopeInterface $envelope = NULL) {
    // $this->conn should be a resource. We use is_null for testability.
    if (!is_null($this->conn) && $this->connected) {
      $comp_code = &$this->compCode;
      $reason = &$this->reason;
      mqseries_disc($this->conn, $comp_code, $reason);
      $this->processCallResult();
      $this->connected = FALSE;
      if ($envelope instanceof EnvelopeInterface) {
        $envelope->setCallResults($this->compCode, $this->reason);
      }
    }
  }

  /**
   * Open an object on the server (normally a queue).
   *
   * @param \GU\mqclient\envelopes\MqodEnvelopeInterface $envelope
   *   The MQOD envelope.
   */
  public function open(MqodEnvelopeInterface $envelope) {
    $mqod = $envelope->getFirstStamp()->toArray();
    $options = $envelope->getOptions();
    $comp_code = &$this->compCode;
    $reason = &$this->reason;
    $object = &$this->openObject;
    mqseries_open($this->conn, $mqod, $options, $object, $comp_code, $reason);
    $this->processCallResult();
    $envelope->setCallResults($this->compCode, $this->reason);
    $envelope->setOpenObject($this->openObject);
  }

  /**
   * Gets a message from MQ.
   *
   * @param \GU\mqclient\envelopes\MqgmoEnvelopeInterface $envelope
   *   The MQGMO envelope.
   *   After the GET call, the retrieved message will be saved in the envelope.
   *   A NULL message means there are no more messages in the queue.
   */
  public function get(MqgmoEnvelopeInterface $envelope) {
    $buffer_length = $envelope->getMaxMessageSize();
    $mqmd = $envelope->getMessageDescriptor()->toArray();
    $mqgmo = $envelope->getFirstStamp()->toArray();
    $data_length = 0;
    $comp_code = &$this->compCode;
    $reason = &$this->reason;
    mqseries_get($this->conn, $envelope->getQueue(), $mqmd, $mqgmo, $buffer_length, $msg, $data_length, $comp_code, $reason);
    $this->processCallResult();
    $envelope->setCallResults($this->compCode, $this->reason);
    if ($this->lastReason() === MQSERIES_MQRC_NO_MSG_AVAILABLE) {
      $envelope->setMessage(NULL, 0);
    }
    else {
      $envelope->setMessage($msg, $data_length);
    }
  }

  /**
   * Commit a unit of work.
   *
   * @param \GU\mqclient\envelopes\EnvelopeInterface $envelope
   *   An envelope, use it when you need to process the call results.
   */
  public function commit(EnvelopeInterface $envelope = NULL): void {
    $comp_code = &$this->compCode;
    $reason = &$this->reason;
    mqseries_cmit($this->conn, $comp_code, $reason);
    $this->processCallResult();
    if ($envelope instanceof EnvelopeInterface) {
      $envelope->setCallResults($this->compCode, $this->reason);
    }
  }

  /**
   * Roll back the last unit of work.
   *
   * @param \GU\mqclient\envelopes\EnvelopeInterface $envelope
   *   An envelope, use it when you need to process the call results.
   */
  public function rollback(EnvelopeInterface $envelope = NULL): void {
    $comp_code = &$this->compCode;
    $reason = &$this->reason;
    mqseries_back($this->conn, $comp_code, $reason);
    $this->processCallResult();
    if ($envelope instanceof EnvelopeInterface) {
      $envelope->setCallResults($this->compCode, $this->reason);
    }
  }

  /**
   * Puts a message in a queue.
   *
   * @param \GU\mqclient\envelopes\MqpmoEnvelopeInterface $envelope
   *   An MQPMO envelope.
   */
  public function put(MqpmoEnvelopeInterface $envelope): void {
    $comp_code = &$this->compCode;
    $reason = &$this->reason;
    mqseries_put($this->conn, $envelope->getQueue(), $envelope->getMessageDescriptor()->toArray(), $envelope->getFirstStamp()->toArray(), $envelope->getMessage(), $comp_code, $reason);
    $this->processCallResult();
    $envelope->setCallResults($this->compCode, $this->reason);
  }

  /**
   * Throw an exception if the last call was not OK.
   *
   * @throws \GU\mqclient\MqException
   */
  protected function processCallResult() {
    if ($this->compCode !== MQSERIES_MQCC_OK) {
      throw new MqException($this->getErrorString(), $this->compCode);
    }
  }

  /**
   * Convert compCode to string.
   *
   * @return string
   *   The string for a compCode.
   */
  protected function compCodeString(): string {
    switch ($this->compCode) {
      case MQSERIES_MQCC_OK:
        return ('SUCCESS');

      case MQSERIES_MQCC_WARNING:
        return ('WARNING');

      case MQSERIES_MQCC_FAILED:
        return('FAILURE');

    }
  }

  /**
   * Convert a formatted error string.
   *
   * @return string
   *   a formatted error string.
   */
  protected function getErrorString(): string {
    return "[" . $this->compCodeString() . " " . $this->reason . "] " . mqseries_strerror($this->reason);
  }

  /**
   * Returns the completion code of the last MQ command.
   *
   * @return int
   *   The completion code of the last MQ command.
   */
  public function lastCode(): int {
    return $this->compCode;
  }

  /**
   * Returns the reason result of the last MQ command.
   *
   * @return int
   *   The last reason returned by MQ.
   */
  public function lastReason(): int {
    return $this->reason;
  }

  /**
   * Returns the reason result of the last MQ command as a string.
   *
   * @return string
   *   The string of the last reason.
   */
  public function lastReasonString(): string {
    return mqseries_strerror($this->reason);
  }

  /**
   * Whether the last command was successful.
   *
   * @return bool
   *   TRUE if the last command was successful.
   */
  public function lastCommandSuccess() : bool {
    return $this->compCode === MQSERIES_MQCC_OK || $this->reason === MQSERIES_MQRC_NO_MSG_AVAILABLE;
  }

  /**
   * Whether the connection is connected.
   *
   * @return bool
   *   True if connected, false if not connected.
   */
  public function isConnected(): bool {
    return $this->connected;
  }

}
